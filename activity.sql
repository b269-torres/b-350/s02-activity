CREATE DATABASE blog_db;

USE blog_db;

CREATE TABLE users(
		id INT NOT NULL AUTO_INCREMENT,
		email VARCHAR(100) NOT NULL,
		password VARCHAR(500) NOT NULL,
		datetime_created DATETIME NOT NULL,
		PRIMARY KEY (id)
		);

CREATE TABLE posts(
	id INT NOT NULL AUTO_INCREMENT,
	users_id INT NOT NULL,
	title VARCHAR(500) NOT NULL,
	content VARCHAR(500) NOT NULL,
	datetime_posted DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_post_user_id
			FOREIGN KEY (users_id) REFERENCES users(id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
		);

CREATE TABLE post_comments(
	id INT NOT NULL AUTO_INCREMENT,
	users_id INT NOT NULL,
	posts_id INT NOT NULL,
	content VARCHAR(500) NOT NULL,
	datetime_posted DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_postpostcomment_user_id
			FOREIGN KEY (users_id) REFERENCES users(id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,
	CONSTRAINT fk_postcomment_post_id
			FOREIGN KEY (posts_id) REFERENCES posts(id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
		);

CREATE TABLE post_likes(
	id INT NOT NULL AUTO_INCREMENT,
	users_id INT NOT NULL,
	posts_id INT NOT NULL,
	datetime_posted DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_postlikes_user_id
			FOREIGN KEY (users_id) REFERENCES users(id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,
	CONSTRAINT fk_postlikes_post_id
			FOREIGN KEY (posts_id) REFERENCES posts(id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
		);